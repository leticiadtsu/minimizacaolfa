package minimizacao;

import java.util.ArrayList;
import java.util.List;

/**
 * @brief Class Machine
 * @author Leticia
 * @date 14/07/2017
 */
public class Machine {

    private ArrayList<State> states;
    private List<Character> alfabeto;
    private List<Transition> transitions;
    private State initial;
    private List<State> finals;

    public Machine(ArrayList<State> states, List<Character> alfabeto, List<Transition> transitions, State initionState, List<State> finals) {
        this.states = new ArrayList<State>();
        this.states = states;
        this.alfabeto = new ArrayList<Character>();
        this.alfabeto = alfabeto;
        this.transitions = new ArrayList<Transition>();
        this.transitions = transitions;
        this.initial = initionState;
        this.finals = new ArrayList<State>();
        this.finals = finals;
    }

    public Machine() {
        this.states = new ArrayList<>();
        this.alfabeto = new ArrayList<>();
        this.transitions = new ArrayList<>();
        this.finals = new ArrayList<>();
    }

    public State getStateDestiny(State stateOrgin, char symbol) {
        for (Transition transition : transitions) {
            if (transition.getStateOrigin() == stateOrgin && transition.getSymbol() == symbol) {
                return transition.getStateDestiny();
            }
        }
        return null;
    }

    public void addState(State state) {
        this.states.add(state);
    }

    public void addStates(ArrayList<State> states) {
        this.states.addAll(states);
    }

    /**
     * @return the states
     */
    public ArrayList<State> getStates() {
        return states;
    }

    public State getState(int index) {
        for (State state : this.states) {
            if (state.getIndex().contains(index)) {
                return state;
            }
        }
        return null;
    }

    /**
     * @param states the states to set
     */
    public void setStates(ArrayList<State> states) {
        this.states = states;

    }

    /**
     * @return
     */
    public List<Character> getAlfabeto() {
        return alfabeto;
    }

    /**
     * @param alfabeto
     */
    public void setAlphabet(List<Character> alfabeto) {
        this.alfabeto = alfabeto;
    }

    /**
     * @return the transitions
     */
    public List<Transition> getTransitions() {
        return transitions;
    }

    /**
     * @param transitions the transitions to set
     */
    public void setTransitions(List<Transition> transitions) {
        this.transitions = transitions;
    }

    /**
     * @return the initialState
     */
    public State getInitionState() {
        return initial;
    }

    /**
     * @param initionState the initialState to set
     */
    public void setInitionState(State initionState) {
        this.initial = initionState;
    }

    /**
     * @return the finals
     */
    public List<State> getFinals() {
        return finals;
    }

    public boolean isFinal(State state) {
        return this.finals.contains(state);
    }

    /**
     * @param finals the finals to set
     */
    public void setFinals(List<State> finals) {
        this.finals = finals;
    }

    private State findState(State stateToFind) {
        for (State state : this.states) {
            System.err.println("quero achar:" + stateToFind.getIndex());
            System.out.println("To procurando em" + state.getIndex());
            for (int indexInState : stateToFind.getIndex()) {
                if (state.getIndex().contains(indexInState)) {
                    System.err.println("Achei um" + state.getIndex() + "\\" + indexInState);
                    return state;
                }
            }
        }
        return null;
    }

    public void join(State state1, State state2) {
        System.err.println(state1.toString() + "/" + state2.toString());
        state1 = this.findState(state1);
        state2 = this.findState(state2);
        System.err.println(state1.toString() + "/" + state2.toString());
        State newState = new State(state1.getIndex());
        newState.addIndex(state2.getIndex());
        ArrayList<Transition> newTransitions = new ArrayList();
        for (Transition transition : this.transitions) {
            boolean remove = false;
            if (transition.getStateOrigin() == state1 || transition.getStateOrigin() == state2) {
                newTransitions.add(new Transition(newState, transition.getStateDestiny(), transition.getSymbol()));
                remove = true;
            }
            if (transition.getStateDestiny() == state1 || transition.getStateDestiny() == state2) {
                newTransitions.add(new Transition(transition.getStateDestiny(), newState, transition.getSymbol()));
                remove = true;
            }
        }
        this.transitions = newTransitions;
    }

    /**
     * Adiciona uma transição no autômato somente se os estados de origem e
     * destino já existem
     *
     * @param transitions
     * @return true se a transição foi adicionada com sucesso, ou false caso
     * contrário
     */
    private boolean addTransition(Transition transition) {
        if (this.states.contains(transition.getStateOrigin()) && this.states.contains(transition.getStateDestiny()) && this.alfabeto.contains(transition.getSymbol())) {
            this.transitions.add(transition);
            return true;
        }
        return false;
    }

    /**
     * Remove uma transição no autômato somente se os estados de origem e
     * destino já existem
     *
     * @param transitions
     * @return true se a transição foi removida com sucesso, ou false caso
     * contrário
     */
    private boolean removeTransition(Transition transition) {
        if (this.states.contains(transition.getStateOrigin()) && this.states.contains(transition.getStateDestiny())) {
            this.transitions.remove(transition);
            return true;
        }
        return false;
    }

    public void addState(String name, int index) {
        this.states.add(new State(index));
    }

    /**
     * remove um estado no autômato e limpa todas as transições contidas neste
     * estado
     *
     * @param transitions
     * @return true se o estado foi removido com sucesso, ou false caso
     * contrário
     */
    private boolean removeState(List<Transition> transitions) {
        return false;
    }

    void addTranstion(Transition transition) {
        this.transitions.add(transition);
    }

    @Override
    public String toString() {
        StringBuilder mac = new StringBuilder("( \n");

        //Estados
        mac.append("{");
        for (State state : this.states) {
            mac.append(state.toString()).append(",");
        }
        mac.append("}, \n");

        //Alfabeto
        mac.append("{");
        mac.append(this.getAlfabeto());
        mac.append("}, \n");

        //Transições
        mac.append("{ \n");
        for (Transition transition : this.transitions) {
            mac.append("(").append(transition.getStateOrigin()).append(",").append(transition.getSymbol()).append("->").append(transition.getStateDestiny()).append("), \n");
        }
        mac.append("}, \n");

        //Estado inicial
        mac.append(this.getInitionState());
        mac.append(", \n");

        //Estados finais
        mac.append("{");
        for (State state : this.getFinals()) {
            mac.append(state.toString()).append(",");
        }
        mac.append("} \n");

        mac.append(")");
        return mac.toString();
    }

}
