package minimizacao;

import java.util.ArrayList;
import java.util.List;

/**
 * @brief Class Machine
 * @author Leticia
 * @date 14/07/2017
 */
public class Machine {

    private ArrayList<State> states;
    private List<Character> alfabeto;
    private List<Transition> transitions;
    private State initial;
    private List<State> finals;

    public Machine(ArrayList<State> states, List<Character> alfabeto, List<Transition> transitions, State initionState, List<State> finals) {
        this.states = new ArrayList<State>();
        this.states = states;
        this.alfabeto = new ArrayList<Character>();
        this.alfabeto = alfabeto;
        this.transitions = new ArrayList<Transition>();
        this.transitions = transitions;
        this.initial = initionState;
        this.finals = new ArrayList<State>();
        this.finals = finals;
    }

    public Machine() {
        this.states = new ArrayList<>();
        this.alfabeto = new ArrayList<>();
        this.transitions = new ArrayList<>();
        this.finals = new ArrayList<>();
    }

    public State getStateDestiny(State stateOrgin, char symbol) {
        for (Transition transition : transitions) {
            if (transition.getStateOrigin() == stateOrgin && transition.getSymbol() == symbol) {
                return transition.getStateDestiny();
            }
        }
        return null;
    }

    public void addState(State state) {
        this.states.add(state);
    }

    public void addStates(ArrayList<State> states) {
        this.states.addAll(states);
    }

    /**
     * @return the states
     */
    public ArrayList<State> getStates() {
        return states;
    }

    public State getState(int index) {
        for (State state : this.states) {
            if (state.getIndex().contains(index)) {
                return state;
            }
        }
        return null;
    }

    /**
     * @param states the states to set
     */
    public void setStates(ArrayList<State> states) {
        this.states = states;

    }

    /**
     * @return
     */
    public List<Character> getAlfabeto() {
        return alfabeto;
    }

    /**
     * @param alfabeto
     */
    public void setAlphabet(List<Character> alfabeto) {
        this.alfabeto = alfabeto;
    }

    /**
     * @return the transitions
     */
    public List<Transition> getTransitions() {
        return transitions;
    }

    /**
     * @param transitions the transitions to set
     */
    public void setTransitions(List<Transition> transitions) {
        this.transitions = transitions;
    }

    /**
     * @return the initialState
     */
    public State getInitionState() {
        return initial;
    }

    /**
     * @param initionState the initialState to set
     */
    public void setInitionState(State initionState) {
        this.initial = initionState;
    }

    /**
     * @return the finals
     */
    public List<State> getFinals() {
        return finals;
    }

    public boolean isFinal(State state) {
        return this.finals.contains(state);
    }

    /**
     * @param finals the finals to set
     */
    public void setFinals(List<State> finals) {
        this.finals = finals;
    }

    private State findState(State stateToFind) {
        for (State state : this.states) {

            for (int indexInState : stateToFind.getIndex()) {
                if (state.getIndex().contains(indexInState)) {
                    return state;
                }
            }
        }
        return null;
    }


    private boolean isToJoin(ArrayList<Integer> index1, ArrayList<Integer> index2) {
        for (int i1 : index1) {
            for (int i2 : index2) {
                if (i1 == i2) {
                    return true;
                }
            }
        }
        return false;
    }

    private void removeRedundancies() {
        ArrayList<Transition> newTransitions = new ArrayList();
        boolean add = true;
        for (int i = 0; i < transitions.size(); i++) {
            if (!newTransitions.contains(transitions.get(i))) {
                newTransitions.add(transitions.get(i));
            }
        }
        this.transitions = newTransitions;

    }

    public void join(State state1, State state2) {
        state1 = this.findState(state1);
        state2 = this.findState(state2);
        State newState = new State(state1.getIndex());
        newState.addIndex(state1.getIndex());
        newState.addIndex(state2.getIndex());

        this.states.remove(state1);
        this.states.remove(state2);
        this.states.add(newState);
        boolean add = false;
        if (this.finals.contains(state1)) {
            this.finals.remove(state1);
            add = true;            
        }
        if (this.finals.contains(state2)) {
            this.finals.remove(state2);
            add = true;            
        }
        if(add){
            this.finals.add(newState);
        }

        for (Transition transition : this.transitions) {
            boolean turn = false;
            if (isToJoin(transition.getStateOrigin().getIndex(), newState.getIndex())) {
                transition.setStateOrigin(newState);
            }
            if (isToJoin(transition.getStateDestiny().getIndex(), newState.getIndex())) {
                transition.setStateDestiny(newState);
            }
        }
        removeRedundancies();

    }


    public void addState(String name, int index) {
        this.states.add(new State(index));
    }

    void addTranstion(Transition transition) {
        this.transitions.add(transition);
    }

    @Override
    public String toString() {
        StringBuilder mac = new StringBuilder("( \n");

        //Estados
        mac.append("\t{");
        for (int i = 0; i < states.size(); i++) {
            mac.append(states.get(i).toString());
            if (i != states.size() - 1) {
                mac.append(",");
            }
        }
        mac.append("}, \n");

        //Alfabeto
        mac.append("\t{");
        mac.append(this.getAlfabeto());
        mac.append("}, \n");

        //Transições
        mac.append("\t{ \n");
        
        for (int i = 0; i < transitions.size(); i++) {
            mac.append("\t\t"+transitions.get(i).toString());
            if (i != transitions.size() - 1) {
                mac.append(",");
            }
            mac.append("\n");
        }
        mac.append("\t}, \n");

        //Estado inicial
        mac.append("\t" + this.getInitionState());
        mac.append(", \n");

        //Estados finais
        mac.append("\t{");
        for (int i = 0; i < finals.size(); i++) {
            mac.append(finals.get(i).toString());
            if (i != finals.size() - 1) {
                mac.append(",");
            }
        }
        mac.append("} \n");

        mac.append(")");
        return mac.toString();
    }

}
