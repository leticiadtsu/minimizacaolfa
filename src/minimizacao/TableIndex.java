/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimizacao;

/**
 *
 * @author Marco
 */
public class TableIndex {

    private State firstIndex;
    private State secondIndex;

    public TableIndex(State firstIndex, State secondIndex) {
        this.firstIndex = firstIndex;
        this.secondIndex = secondIndex;
    }

    /**
     * @return the firstIndex
     */
    public State getFirstState() {
        return firstIndex;
    }

    /**
     * @return the secondIndex
     */
    public State getSecondState() {
        return secondIndex;
    }

    @Override
    public String toString() {
        int first = firstIndex.getIndex().get(0);
        int second = secondIndex.getIndex().get(0) ;
        if(first > second){
            int aux = first;
            first = second;
            second = aux;
        }
        return "[" + first + "," + second + "]";
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        if (obj != null && this.getClass() == obj.getClass()) {
            TableIndex aux = (TableIndex) obj;
            
            if (this.getFirstState() == aux.getFirstState() && this.getSecondState() == aux.getSecondState()) {
            
                return true;
            }
        }
        return false;
    }

}
