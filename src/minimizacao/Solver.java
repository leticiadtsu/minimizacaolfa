/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimizacao;

/**
 *
 * @author Marco
 */
public class Solver {

    private Table table;
    private Machine mac;
    private String nameArqOutTable;
    private String nameArqOutMachine;

    public Solver(String arqDescription, String arqOutTable, String arqOutMachine) {
        FileManager fileMananger = FileManager.getInstance();
        this.mac = fileMananger.read(arqDescription);
        this.nameArqOutMachine = arqOutMachine;
        this.nameArqOutTable = arqOutTable;
    }

    public void start() {
        this.table = new Table(mac);
    }

    public Table getTable() {
        return this.table;
    }

    public Machine getMachine() {
        return this.mac;
    }

    private void distinctFinalNotFinal() {
        Line line;
        for (int i = 0; i < table.size(); i++) {
            line = table.getLine(i);
            TableIndex index = line.getIndex();
            if (mac.isFinal(index.getFirstState()) != mac.isFinal(index.getSecondState())) {
                this.table.turnDistintc(index, "final/nao-final");
            }
        }
    }

    private void testLine(Line line) {
        State state1 = line.getIndex().getFirstState();
        State state2 = line.getIndex().getSecondState();
        TableIndex destiny;
        State stateDestiny1;
        State stateDestiny2;
        for (char symbol : mac.getAlfabeto()) {
            stateDestiny1 = mac.getStateDestiny(state1, symbol);
            stateDestiny2 = mac.getStateDestiny(state2, symbol);
            TableIndex indexDestiny = new TableIndex(stateDestiny1, stateDestiny2);
            if (table.isDistinct(indexDestiny)) {
                table.turnDistintc(line.getIndex(), symbol + indexDestiny.toString());
                return;
            } else if (indexDestiny.getFirstState() != indexDestiny.getSecondState() && !indexDestiny.equals(line.getIndex())) {

                table.addDependence(indexDestiny, line.getIndex());
            }
        }
    }

    private void joinStates() {
        Line line;
        for (int i = 0; i < table.size(); i++) {
            line = table.getLine(i);
            if (!line.isDistinct()) {
                TableIndex index = line.getIndex();
                mac.join(index.getFirstState(), index.getSecondState());
            }
        }

    }

    private void solveTable() {
        this.distinctFinalNotFinal();
        Line line;
        for (int i = 0; i < table.size(); i++) {
            line = table.getLine(i);

            if (!line.isDistinct()) {

                testLine(line);
            }
        }
    }

    public void solve() {
        this.table = new Table(this.mac);
        this.solveTable();
        this.joinStates();
        FileManager.getInstance().write(this.nameArqOutMachine, this.nameArqOutTable, this.mac, this.table);
    }

}
