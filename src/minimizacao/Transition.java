package minimizacao;

/**
 * @brief Class Transition
 * @author Leticia
 * @date 14/07/2017
 */
public class Transition {

    private State stateOrigin;
    private State stateDestiny;
    private char symbol;

    public Transition(State stateOrigin, State stateDestiny, char symbol) {
        this.stateOrigin = stateOrigin;
        this.stateDestiny = stateDestiny;
        this.symbol = symbol;
    }

    /**
     * @return the stateOrigin
     */
    public State getStateOrigin() {
        return stateOrigin;
    }

    /**
     * @param stateOrigin the stateOrigin to set
     */
    public void setStateOrigin(State stateOrigin) {
        this.stateOrigin = stateOrigin;
    }

    /**
     * @return the stateDestiny
     */
    public State getStateDestiny() {
        return stateDestiny;
    }

    /**
     * @param stateDestiny the stateDestiny to set
     */
    public void setStateDestiny(State stateDestiny) {
        this.stateDestiny = stateDestiny;
    }

    /**
     * @return the symbol
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "(" + this.stateOrigin.toString() + "," + this.symbol + "->" + this.stateDestiny.toString() + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Transition other = (Transition) obj;
        
        if (other.getStateDestiny().equals(this.getStateDestiny()) && other.getSymbol() == this.getSymbol() && other.getStateOrigin().equals(this.getStateOrigin())) {
            return  true;
        }
        return false;
    }
}
