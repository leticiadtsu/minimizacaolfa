/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimizacao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Marco
 */
public class FileManager {

    private static FileManager instance = null;

    private FileManager() {

    }

    public static FileManager getInstance() {
        if (instance == null) {
            instance = new FileManager();
        }
        return instance;
    }

    private ArrayList<State> readStates(String line) {
        line = line.trim();
        line = line.replace("{", "").replace("}", "");
        String[] stringStates = line.split(",");
        ArrayList<State> states = new ArrayList();
        for (String state : stringStates) {

            state = state.replace("q", "");
            states.add(new State((new Integer(state))));
        }
        return states;
    }

    private ArrayList<Character> readAlphabet(String line) {
        line = line.trim();
        line = line.replace("{", "").replace("}", "");
        String[] stringAlphabet = line.split(",");
        ArrayList<Character> alphabet = new ArrayList();

        for (String symbol : stringAlphabet) {

            alphabet.add(symbol.charAt(0));
        }
        return alphabet;
    }

    public boolean validateFormat(String line) {
        if (line.length() == 1 && line.contains("(")) {
            return true;
        }
        return false;
    }

    public Transition readTransitions(String lineFile, Machine mac) {
        lineFile = lineFile.replace("(", "").replace(")", "");
        String[] aux = lineFile.split("->");
        String stringDestinyState = aux[1];
        stringDestinyState = stringDestinyState.replace(",", "").replace("q", "");
        State destinyState = mac.getState(new Integer(stringDestinyState));
        String originStateAndSybol = aux[0];
        String[] aux2 = originStateAndSybol.split(",");
        String stringOriginState = aux2[0];
        stringOriginState = stringOriginState.replace("q", "");
        State originState = mac.getState(new Integer(stringOriginState));
        char symbol = aux2[1].charAt(0);

        Transition trasition = new Transition(originState, destinyState, symbol);

        return trasition;
    }

    private State readInitialState(String line, Machine mac) {
        line = line.replace(",", "").replace("q", "");

        return mac.getState(new Integer(line));
    }

    private ArrayList<State> readFinalStates(String line, Machine mac) {
        line = line.trim();
        line = line.replace("{", "").replace("}", "");
        String[] stringStates = line.split(",");
        ArrayList<State> states = new ArrayList();
        for (String state : stringStates) {
            state = state.replace("q", "");
            states.add(new State((new Integer(state))));
        }
        return states;
    }

    public Machine read(String nameFile) {
        Machine mac = new Machine();
        try {
            FileReader file = new FileReader(nameFile);
            BufferedReader bf = new BufferedReader(file);
            String lineFile = bf.readLine().trim();
            if (validateFormat(lineFile)) {
                lineFile = bf.readLine().trim();
                mac.addStates(readStates(lineFile));
                lineFile = bf.readLine().trim();
                mac.setAlphabet(readAlphabet(lineFile));
                lineFile = bf.readLine().trim();
                if (lineFile.length() == 1 && lineFile.contains("{")) {
                    lineFile = bf.readLine().trim();
                    while (!lineFile.contains("},")) {
                        mac.addTranstion(this.readTransitions(lineFile, mac));
                        lineFile = bf.readLine().trim();
                    }
                    lineFile = bf.readLine().trim();
                    State initialState = this.readInitialState(lineFile, mac);
                    mac.setInitionState(initialState);
                    lineFile = bf.readLine().trim();
                    mac.setFinals(this.readFinalStates(lineFile, mac));
                }
            }
            bf.close();
        } catch (IOException e) {

        }
        return mac;
    }

    public void write(String nameOutFileMachine, String nameFileOutTable, Machine mac, Table table) {
        try {
            FileWriter file = new FileWriter(nameOutFileMachine);
            BufferedWriter bw = new BufferedWriter(file);
            bw.append(mac.toString());
            bw.flush();
            bw.close();
           
             
            FileWriter file1 = new FileWriter(nameFileOutTable);
            BufferedWriter bw1 = new BufferedWriter(file1);
            bw1.append(table.toString());
            bw1.flush();
            bw1.close();
            
        } catch (Exception e) {

        }
    }
}
