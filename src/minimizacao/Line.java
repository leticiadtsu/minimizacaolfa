package minimizacao;

import java.util.ArrayList;
import java.util.List;

/**
 * @brief Class Line
 * @author Leticia
 * @date 14/07/2017
 */
public class Line {

    /**
     * @return the index
     */
    public TableIndex getIndex() {
        return index;
    }

    /**
     * Apenas recebe o indice que é combinação de dois estados, e assume
     * inicialmente que não são distintos e é vazio a causa e a lista de
     * dependencias
     *
     * @param indice0
     * @param indice1
     */
    public Line(State indice0, State indice1) {
        this.index = new TableIndex(indice0, indice1);
        this.distinct = false;
        this.cause = "";
        this.dependents = new ArrayList<>();
    }

    /**
     * @return the distinct
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * @param distinct the distinct to set
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * @return the cause
     */
    public String getCause() {
        return cause;
    }

    /**
     * @param cause the cause to set
     */
    public void setCause(String cause) {
        this.cause = cause;
    }

    /**
     * @return the dependents
     */
    public List<TableIndex> getDependents() {
        return dependents;
    }

    private TableIndex index;
    private boolean distinct;
    private List<TableIndex> dependents;
    private String cause;

    public void addDependent(TableIndex dependent) {
        this.getDependents().add(dependent);
    }

    @Override
    public String toString() {
        String distintcCode = "0";
        if (this.distinct) {
            distintcCode = "1";
        }
        StringBuilder stringDependents = new StringBuilder("{");
        for (int i = 0; i < this.getDependents().size(); i++) {
            stringDependents.append(this.getDependents().get(i).toString());
            if (i != this.getDependents().size() - 1) {
                stringDependents.append(",");
            }
        }
        stringDependents.append("}");
        return this.getIndex().toString() + "\t" + distintcCode + "\t" + stringDependents.toString() + "\t" + this.cause;

    }
}
