/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimizacao;

/**
 *
 * @author Marco
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String arqDescription = args[0];
        String arqOutTable = args[1];
        String arqOutMachine = args[2];
        Solver solver = new Solver(arqDescription, arqOutTable, arqOutMachine);
        solver.solve();
    }

}
