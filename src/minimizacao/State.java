package minimizacao;

import java.util.ArrayList;
import java.util.Collections;

public class State implements Comparable<State>{

    private ArrayList<Integer> index;

    public State(int index) {
        this.index = new ArrayList();
        this.index.add(index);
//        Collections.sort(this.index);

    }

    public State(ArrayList<Integer> index) {
        this.index = new ArrayList(index);
        Collections.sort(this.index);
    }

    /**
     * @return the indice
     */
    public ArrayList<Integer> getIndex() {
        return index;
    }

    public void addIndex(ArrayList<Integer> index) {
        for (int i : index) {
            this.addIndex(i);
        }
    }

    public void addIndex(int index) {
        if (!this.index.contains(index)) {
            this.index.add(index);
            Collections.sort(this.index);
        }

    }

    public void setIndex(ArrayList<Integer> index) {
        this.index = index;
    }

    /**
     * Sobrescrita do método equals. Na classe State, são iguais apenas quando o
     * name e indice possuirem valores equivalentes.
     *
     * @return retorna verdadeiro se o indice e o name do estado for igual
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        State other = (State) obj;
        if (this.index.size() == other.index.size()) {
            for (int i = 0; i < this.index.size(); i++) {
                if (this.index.get(i) != other.index.get(i)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder state = new StringBuilder("q");
        for (int i = 0; i < this.index.size(); i++) {
            if (i > 0) {
                state.append("q");
            }
            state.append(this.index.get(i).toString());
        }
        return state.toString();
    }

    @Override
    public int compareTo(State t) {
        System.out.print("vou testar");
        if(this.equals(t)){
            return 0;
        }
        else return -1;
    }
    

}
