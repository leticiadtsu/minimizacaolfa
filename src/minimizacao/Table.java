/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimizacao;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Marco
 */
public class Table {

    private ArrayList<Line> lines;
    private HashMap<String, Integer> map;

    public Table(Machine mac) {
        this.lines = new ArrayList();
        this.map = new HashMap();
        this.create(mac);
    }

    private void create(Machine mac) {
        ArrayList<State> states = mac.getStates();
        TableIndex index;
        for (int i = 0; i < states.size() - 1; i++) {
            for (int j = i + 1; j < states.size(); j++) {
                index = new TableIndex(states.get(i), states.get(j));
                this.lines.add(new Line(index.getFirstState(), index.getSecondState()));
                this.map.put(index.toString(), lines.size()-1);
            }
        }

    }

    public void addDependence(TableIndex index, TableIndex dependent) {
        Line line = this.getLine(index);
        line.addDependent(dependent);
    }

    public void turnDistintc(TableIndex index, String cause) {

        Line line = this.getLine(index);

        line.setDistinct(true);
        line.setCause(cause);
        for (TableIndex i : line.getDependents()) {
            
            turnDistintc(i, "prop" + index.toString());
        }
    }

    public Line getLine(TableIndex index) {
        if (this.map.get(index.toString()) == null) {
            index = new TableIndex(index.getSecondState(), index.getFirstState());
        }
        int pos = this.map.get(index.toString());
        return lines.get(pos);
    }

    public boolean isDistinct(TableIndex index) {
        if (index.getFirstState() == index.getSecondState()) {
            return false;
        }
        Line line = this.getLine(index);
        return line.isDistinct();
    }

    public Line getLine(int index) {
        return lines.get(index);
    }

    public int size() {
        return lines.size();
    }

    @Override
    public String toString() {
        StringBuilder table = new StringBuilder("INDICE\tD[i,j] =\tS[i,j] =\tMOTIVO\n");
        for (Line line : lines) {
            table.append(line.toString() + "\n");
        }
        return table.toString();
    }
}
